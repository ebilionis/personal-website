# README #

This code builds the website of the PredictiveScience Lab of the School of Mechanical Engineering, Purdue University led by Prof. Bilionis. All you need to compile the site is sphinx (Python, and a few other packages). Assuming you have what you need, download the code and run:

 $ make html