.. Ilias Bilionis documentation master file, created by
   sphinx-quickstart on Sun Oct  6 16:53:41 2013.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. _welcome:

*********************
PredictiveScience Lab
*********************

.. image:: _images/ib_small.png
    :width: 200px
    :align: left
    :alt: alternate text

Physical phenomena are characterized by models spanning a wide range of spatio-temporal scales (multi-scale) as well as scientific principles (multi-physics). Carrying out engineering tasks –such as uncertainty quantification and design- using the full complexity of these models faces insurmountable computational barriers. Typical examples include geological flows, climate modeling, mutli-scale material modeling, protein folding, etc.
The ultimate goal of our research is to make feasible the routine use of
complex realistic models by scientists and engineers.
There are two –seemingly- different research areas which, we believe,
are necessary in order to accomplish this goal.
Firstly, new mathematical principles must be developed that allow us to
bridge the gaps between model layers (coarse-graining).
Secondly, computationally expensive models have to be replaced by cheaper alternatives.

.. image:: _images/pme.png
    :width: 200px
    :align: right
    :alt: alternate text

:ref:`bilionis`, e:download:`CV <ib_cv.pdf>`, bilionis [at] gmail.com

Read more in our :ref:`research` and :ref:`projects` pages.

.. _news:

News
----

+ Prospective students should check out :ref:`open_positions`.
+ Our work on "Data-driven model for solar irradiation based on satellite observations" was accepted for publication in Solar Energy.
  Visit the website containing hundreds of forecasts `here <http://www.mcs.anl.gov/~ebilionis/solar-irradiance/>`_. The code that generates everything is `here <https://bitbucket.org/ebilionis/solar-irradiation>`_.

.. _selected_publications:

Selected Publications
---------------------

+ I. Bilionis, M. Anitescu and E. M. Constantinescu, Data-driven model for
  solar irradiation based on satellite observations.
  *Solar Energy (accepted)*
+ I. Bilionis and N. Zabaras. Solution of inverse problems with limited
  forward solver evaluations: A fully Bayesian perspective, Inverse
  Problems, 30 015004, 2014. A copy can be found
  `here <http://iopscience.iop.org/0266-5611/30/1/015004>`_.
+ I. Bilionis and N. Zabaras. Solution of inverse problems with limited
  forward solver evaluations: A fully Bayesian perspective,
  *Inverse Problems, 30:015004*
  , 2014. A copy can be found
  `here <http://iopscience.iop.org/0266-5611/30/1/015004>`_.
+ I. Bilionis and N. Zabaras, A stochastic optimization approach to coarse-graining using a relative-entropy framework. 
  *The Journal of Chemical Physics*
  , 138, 044313, 2013. A copy can be found
  `here <http://link.aip.org/link/?JCP/138/044313>`_.
+ J. T. Kristensen*, I. Bilionis, and N. Zabaras. Relative entropy as model
  selection tool in cluster expansions.
  *Physical Review B.*
  , 87, 174112,
  2013. A copy can be found
  `here <http://link.aip.org/link/?JCP/138/044313>`_.
+ I. Bilionis and N. Zabaras. Multi-output local Gaussian process
  regression: Applications to uncertainty quantification.
  *Journal of Computational Physics*
  , 231:5718–5746, 2012.
  A copy can be found
  `here <http://www.sciencedirect.com/science/article/pii/S0021999112002513>`_.


For the complete list see our :ref:`publications`
or our `Google Scholar profile <http://scholar.google.com/citations?hl=en&authuser=1&user=rjXLtJMAAAAJ>`_.


.. _selected_software:

Selected Opensource Software
----------------------------

+ :ref:`pysolar`.
+ :ref:`pybest`.
+ :ref:`pysmc`.
+ :ref:`personal_web`!

For more, visit our :ref:`software` page.

.. toctree::
    :hidden:
    :maxdepth: 2

    people
    open_positions
    publications
    research
    projects
    software
    teaching
