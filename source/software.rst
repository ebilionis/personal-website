.. _software:

Software
========

This is an (incomplete) list of open source software that we have developed.
It is basically a blend of Fortran, C and C++ code interfaced via Python. Our
motive is to reuse as much of as possible of existing codes to create easy-to-use
Python tools for carrying out uncertainty quantification tasks and the solving 
inverse problems. Here you go:

.. _pybest:

PyBest (Python Bayesian Exploration Statistical Toolbox)
--------------------------------------------------------

This is an uncertainty quantification package that I am constantly developing. It has
code from all our UQ papers. It features interfaces to various legacy codes for
constructing good designs and quasi-random sequences, easy construction of
polynomials orthogonal with respect to arbitrary probability densities,
Multidimensional Relevance Vector Machines, Multidimensional Gaussian Process
Regression, etc. The documentation is by no means complete, but the package is always under active development.
Get the most recent version of the code from the
`PyBest Github page <https://github.com/ebilionis/py-best>`_ or simply do a::

    $ git clone https://github.com/ebilionis/pysmc.git

The documentation can be found
`here <http://ebilionis.github.io/py-best/html/>`_.

.. _pysmc:

PySmc (Python Sequential Monte Carlo)
-------------------------------------

This is a very convenient
Python-only package that implements an adaptive, parallel version of
Sequential Monte Carlo (SMC). SMC is the tool of choice when you need to
sample from multi-modal probability densities. It is the tool of choice when
it comes to sovlving inverse problems using a Bayesian formalism. It can also
be used to train abritrary probabilistic models in parallel (e.g., Gaussian
processes). Get the most recent version of the code from the
`PySmc Github page <https://github.com/ebilionis/pysmc>`_ or simply do a::

    $ git clone https://github.com/ebilionis/pysmc.git

The documentation can be found
`here <http://ebilionis.github.io/pysmc/>`_.

.. _pyorthpol:

Py-ORTHPOL (Orthogonal Polynomials in Python)
---------------------------------------------

The ``py-ORTHPOL`` package defines the module ``orthpol`` which can be used to
easily construct univariate and multivariate orthogonal polynomials in Python.
The polynomials may be used in variaous other Python codes. For example,
+ in least squares applications,
+ to define the mean in Gaussian process regression,

The need to have an easy to use package that can generate polynomials orthogonal
with respect to arbitrary weight functions is motivated by applications in the
field of Uncertainty Quantification (UQ). In UQ, collections of such polynomials
are known as generalized Polynomial Chaos (gPC). The end goal is to provide
a tool that makes it **ridiculously easy** to construct these polynomials.
Get the most recent version of the code from the
`Py-ORTHPOL Github page <https://github.com/ebilionis/py-orthpol>`_ or simply do
a::
    
    $ git clone https://github.com/ebilionis/py-orthpol.git

.. _pydesign:

Py-Design (Design of Experiments in Python)
-------------------------------------------

The ``py-design`` package defines the Python module ``design`` which implements
several routines for the design of experiments. Basically, it serves as
a wrapper the Fortran 90 codes for experimental design written by 
[John Burkardt](http://people.sc.fsu.edu/~jburkardt/). We have collected, 
probably, all of them here.
Get the most recent version of the code from the
`Py-Design Github page <https://github.com/ebilionis/py-design>`_ or simply
do a::

    $ git clone https://github.com/ebilionis/py-design.git

.. _pymcmc:

Py-MCMC (Some generic MCMC routines for Python)
-----------------------------------------------

The main purpose of this module is to serve as a simple MCMC framework for
generic models. Probably the most useful contribution at the moment, is that
it can be used to train Gaussian process (GP) models implemented in the 
[GPy package](http://sheffieldml.github.io/GPy/).

The code features the following things at the moment:
+ Fully object oriented. The models can be of any type as soon as they offer
  the right interface.
+ Random walk proposals.
+ Metropolis Adjusted Langevin Dynamics.
+ The MCMC chains are stored in fast [HDF5](http://www.hdfgroup.org/HDF5/)
  format using [PyTables](http://www.pytables.org/moin).
+ A mean function can be added to the (GP) models of the [GPy package](http://sheffieldml.github.io/GPy/).

Get the most recent version of the code from the
`Py-MCMC Github page <https://github.com/ebilionis/py-mcmc>`_ or simply do a::

    $ git clone https://github.com/ebilionis/py-mcmc.git

.. _pysolar:

Py-Solar (Data-Driven Model for Solar Irradiation Based on Satellite Observations)
----------------------------------------------------------------------------------

This code implements the "Data-driven model for solar irradiation based on satellite observations" (Bilionis et al., Solar Energy, 2014). 
Please consult the paper for the details of the implementation.
Get the latest version of the code from the
`Py-Solar Bitbucket page <https://bitbucket.org/ebilionis/solar-irradiation>`_ or simply do a::

    $ git clone https://ebilionis@bitbucket.org/ebilionis/solar-irradiation.git

.. _personal_web:

The code that builds this website
---------------------------------

This is the code that builds the website you are looking at right now.
I find it extremely bothersome to write in html.
Therefore, I write in restructured `reST <http://rest-sphinx-memo.readthedocs.org/en/latest/index.html>`_, and I let `Sphinx <http://sphinx-doc.org>`_ convert it to
html.
Get the latest version of the code from the
`Bitbucket page <https://bitbucket.org/ebilionis/personal-website>`_ or simply do a::

    $ git clone https://ebilionis@bitbucket.org/ebilionis/personal-website.git


