.. _climate:

Climate Science for a Sustainable Energy Future
===============================================

This study is part of the `CSSEF <http://climate.llnl.gov/cssef/>`_ project and is focused on the Crop sub-component of the land model (CLM). The CLM-Crop model is a complex system that relies on a suite of parametric inputs that govern plant growth under a given atmospheric forcing and available resources. CLM-Crop development used measurements GPP and NEE from AmeriFlux sites to choose parameter values that optimize crop productivity in the model.

More details coming soon...
