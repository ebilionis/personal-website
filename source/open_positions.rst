.. _open_positions:

==============
Open Positions
==============

You are welcome to visit this page periodically in order to keep up to date
with our current open positions. In any case, if you are interested in our
group, do not hesitate to contact Prof. Bilionis directly at
ebilionis [at] gmail.com.


.. _gs_openings:

Graduate Students
-----------------

The group has openings for two highly motivated PhD students.
Prospective students must have an interest in energy and/or material science
applications from a computational point of view (e.g., design and optimization
under uncertainty, reliability analysis, calibration of missing parameters,
data-driven models).
The advantage of these positions is that they offer a truly interdisciplinary
training with emphasis on predictive and computational science.
The selected students will have the opportunity to interact with
scientists and engineers from many different fields. The training they will obtain
will make them highly competitive for predictive science positions both in academia
and the industry.
If you are interested, contact Prof. Bilionis at ebilionis [at] gmail.com directly.

.. _postdoc_openings:

Postdoc Openings
----------------

Unfortunately, there are no Postdoc openings at the moment.
Please, visit this page at a later time.

