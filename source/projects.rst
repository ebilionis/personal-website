.. _projects:

Projects
========

Here is an overview of the projects I am working on or have worked
in the past:

.. toctree::
    :maxdepth: 2

    climate

More projects coming soon...
