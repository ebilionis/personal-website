.. _publications:

Publications
============

.. _refereed_journal_publications:

Refereed Journal Publications
-----------------------------

2014
^^^^

+ I. Bilionis, B. A. Drewniak and E. M. Constantinescu, Crop physiology
  calibration in CLM. *Geoscientific Model Development (submitted)*, 2014.
+ P. Chen*, N. Zabaras and I. Bilionis, Variational Bayesian inference with
  infinite mixture of Gaussian processes for uncertainty quantification.
  *Journal of Computational Physics (submited)*
  , 2014.
+ I. Bilionis, M. Anitescu and E. M. Constantinescu, Data-driven model for
  solar irradiation based on satellite observations.
  *Solar Energy (accepted)*
  , 2014.
+ I. Bilionis and N. Zabaras. Solution of inverse problems with limited
  forward solver evaluations: A fully Bayesian perspective,
  *Inverse Problems, 30:015004*
  , 2014. A copy can be found
  `here <http://iopscience.iop.org/0266-5611/30/1/015004>`_.

2013
^^^^
+ J. T. Kristensen*, I. Bilionis, and N. Zabaras. Relative entropy as model
  selection tool in cluster expansions.
  *Physical Review B.*
  , 87, 174112,
  2013. A copy can be found
  `here <http://link.aip.org/link/?JCP/138/044313>`_.
+ I. Bilionis and N. Zabaras, A stochastic optimization approach to coarse-graining using a relative-entropy framework. 
  *The Journal of Chemical Physics*
  , 138, 044313, 2013. A copy can be found
  `here <http://link.aip.org/link/?JCP/138/044313>`_.
+ I. Bilionis, N. Zabaras, B. A. Konomi, and G. Lin. Multi-output separable
  Gaussian process: Towards an efficient, fully Bayesian paradigm for
  uncertainty quantification.
  *Journal of Computational Physics*
  ,241:212-239, 2013. A copy can be found
  `here <http://www.sciencedirect.com/science/article/pii/S0021999113000417>`_.

2012
^^^^
+ I. Bilionis and N. Zabaras. Multidimensional adaptive relevance vector
  machines for uncertainty quantification.
  *SIAM Journal for Scientific Computing*
  , Vol. 34, No. 6, pp. B881–B908 2012.
  A copy can be found
  `here <http://epubs.siam.org/doi/pdf/10.1137/120861345>`_.
+ I. Bilionis and N. Zabaras. Multi-output local Gaussian process
  regression: Applications to uncertainty quantification.
  *Journal of Computational Physics*
  , 231:5718–5746, 2012.
  A copy can be found
  `here <http://www.sciencedirect.com/science/article/pii/S0021999112002513>`_.
+ I. Bilionis and P. S. Koutsourelakis. Free energy computations by
  minimization of Kullback-Leibler divergence: An efficient adaptive biasing potential method for sparse representations.
  *Journal of Computational Physics*
  , 231:3849–3870, 2012. A copy can be found
  `here <http://www.sciencedirect.com/science/article/pii/S0021999112000630>`_.

2011
^^^^
+ P. S. Koutsourelakis and I. Bilionis. Scalable Bayesian reduced-order
  models for simulating highdimensional multiscale dynamical systems.
  *Multiscale Modeling and Simulation*
  , 9(1):449–485, 2011. A copy can be
  found `here <http://epubs.siam.org/doi/abs/10.1137/100783790?journalCode=mmsubt>`_.

The '*' stands for PhD students I have mentored.

Look also at my
`Google Scholar profile <http://scholar.google.com/citations?hl=en&authuser=1&user=rjXLtJMAAAAJ>`_.



.. _conferences:

Conferences
-----------

2014
^^^^

+ AGU Fall Meeting, San Francisco, CA, USA, December 15-19, 2014. Soybean
  physiology calibration in the community land model.
+ SIAM Conference on Uncertainty Quantification, Savannah, GA, USA,
  March 31-April 3, 2014. Solution of inverse problems with limited forward
  solver evaluations.

2013
^^^^

+ SIAM Conference on Computational Science and Engineering, Boston, MA, USA,
  February 25-March 1, 2013. Building surrogates of very expensive computer
  codes: Applications to uncertainty quantification.
+ SIAM Conference on Computational Science and Engineering, Boston, MA, USA,
  February 25-March 1, 2013. Solution of inverse problems with limited
  forward solver evaluations: A fully Bayesian framework.
+ 12th U.S. National Congress on Computational Mechanics, Raleigh, NC, USA,
  July 22-25, 2013. Multidimensional adaptive relevance vector machines for
  uncertainty quantification.
+ 12th U.S. National Congress on Computational Mechanics, Raleigh, NC, USA,
  July 22-25, 2013. Relative entropy based surrogate energy models for modeling
  phase transitions.
+ 12th U.S. National Congress on Computational Mechanics, Raleigh, NC, USA,
  July 22-25, 2013. Solution of inverse problems with limited forward solver
  evaluations: A Bayesian framework.

2012
^^^^

+ SIAM Conference on Uncertainty Quantification, Raleigh, NC, April 2-5,
  2012. Sparse Bayesian techniques for surrogate creation and uncertainty
  quantification.
+ SIAM Conference on Uncertainty Quantification, Raleigh, NC, April 2-5,
  2012. Hierarchical multi-output Gaussian process regression for
  uncertainty quantification with arbitrary input probability
  distributions.
+ SIAM Conference on Uncertainty Quantification, Raleigh, NC, April 2-5,
  2012. Uncertainty Quantification with High Dimensional, Experimentally
  Measured Inputs. 
+ Cornell University, SCAN, Ithaca, NY, March 5, 2012. Bayesian techniques
  for uncertainty quantification of computer codes.

2011
^^^^
+ USA/Brazil Symposium on Stochastic Modeling and Uncertainty
  Quantification, Rio de Janeiro, Brazil, August 1-5, 2011. Multi-output
  Local Gaussian Process Regression: Applications to Uncertainty
  Quantification.
+ 11th U.S. National Congress on Computational Mechanics, Minneapolis, MN,
  July 25-29, 2011. Sparse Bayesian Kernel Techniques for the Solution of
  SPDEs.
+ SIAM Computational Science and Engineering Meeting, Reno, NV,
  February 28-March 2, 2011. Kernel PCA for Stochastic Input Generation
  of Multiscale Systems. 

2010
^^^^
+ Cornell University, LASSP, Ithaca, NY, June 17, 2010. Learning free
  energy surfaces by minimizing a Kullback-Leibler divergence.
+ SIAM Conference on Mathematical Aspects of Materials Science,
  Philadelphia, PA, May 23-26 2010. Adaptive free energy calculations for
  crystalline materials.
+ IV European Conference on Computational Mechanics, Paris, France, 
  May 16-21 2010. Coarse-graining in crystalline materials through adaptive
  free-energy calculations.

Invited Talks
-------------

2014
^^^^

+ Mathematics and Computer Science Division, Argonne National Laboratory, Lemont,
  IL, USA, June 5, 2014. Data-driven model for solar irradiation based on satellite
  observations.

2013
^^^^

+ School of Mechanical Engineering, Purdue University, West Lafayette, IN, USA,
  July 17, 2013. Towards a data-driven, fully Bayesian framework for
  uncertainty quantification.
+ Mathematics and Computer Science Division, Argonne National Laboratory, Lemont,
  IL, USA, May 1, 2013. Towards a data-driven, fully Bayesian framework for
  uncertainty quantification.
+ Department of Industrial Engineering, University of Pittsburgh, Pittsburgh, PA,
  USA, April 5, 2013. Towards a data-driven, fully Bayesian framework for
  uncertainty quantification.

2012
^^^^

+ Scientific Computing and Numerics Seminar, Cornell University, Ithaca, NY, USA,
  March 5, 2012. Bayesian techniques for uncertainty quantification of computer
  codes.

2011
^^^^

+ USA/Brazil Symposium on Stochastic Modeling and Uncertainty Quantification, Rio
  de Janeiro, Brazil, August 1-5, 2011. Multi-output local Gaussian process
  regression: Applications to uncertainty quantification.

2010
^^^^

+ Laboratory of Atomic and Solid State Physics, Cornell University, Ithaca, NY,
  USA, June 17, 2010. Learning free energy surfaces by minimizing a
  Kullback-Leibler divergence.
