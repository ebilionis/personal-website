.. _teaching:

Teaching
========

ME 270, Fall 2014 (Purdue University)
-------------------------------------

Statics I.

MAE 7140, Spring 2013, (Cornell University)
-------------------------------------------

Bayesian Scientific Computing, taught by Prof. Zabaras.
I co-designed syllabus, prepared lecture notes, and I substitute lecturer.

MAE 7150, Spring 2012, (Cornell University)
------------------------------------------

Atomistic Modeling of Materials, taught by Prof. Zabaras.
I was a substitute lecturer.

MAE 4700/5700, Fall 2011, (Cornell University)
---------------------------------------------

Finite Element Analysis for Mechanical and Aerospace Engineers, taught by
Prof. Zabaras.
I was a substitute lecturer.

