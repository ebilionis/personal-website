.. _people:

People
======

.. _bilionis:

Prof. Bilionis
--------------

.. image:: _images/ib_small.png
    :width: 200px
    :align: left
    :alt: alternate text

Dr. Ilias Bilionis is an Assistant Professor at the
`School of Mechanical Engineering <https://engineering.purdue.edu/ME>`_,
`Purdue University <http://www.purdue.edu>`_.
His research is motivated by energy and material science applications and it
focuses on the development of generic methodologies for design and optimization
under uncertainty, reliability analysis, model calibration and learning models
out of data.
Prior to his appointment at Purdue he was a Postdoctoral Researcher at the
Mathematics and Computer Science Division (MCS), Argonne National Laboratory.
He received his PhD in Applied Mathematics from Cornell University in 2013 and
his Diploma in Applied Mathematics from the National Technical University of Athens in 2008.

.. image:: _images/cu_logo.png
    :width: 100px
    :align: right
    :alt: alternate text

Education
^^^^^^^^^

+ PhD in `Applied Mathematics <http://www.cam.cornell.edu/>`_,
  `Cornell University <http://www.cornell.edu>`_, Ithaca, USA, 2013
    - Concentrations: Mathematics and Computational Science and Engineering
    - Adivsor: `Nicholas Zabaras <http://mpdc.mae.cornell.edu/People/ZabarasCv/Zabaras.html>`_,
      `Materials Process Design and Control Laboratory <http://mpdc.mae.cornell.edu/>`_,
      `Sibley School of Mechanical and Aerospace Engineering <http://www.mae.cornell.edu/>`_.
    - Thesis: `Bayesian Methods for Uncertainty Quantification <http://ecommons.cornell.edu/handle/1813/34013>`_.

.. image:: _images/ntua_logo.png
    :width: 100px
    :align: right
    :alt: alternate text

+ Diplomma in `Applied Mathematics <http://www.semfe.ntua.gr>`_,
  `National Technical University of Athens <http://www.ntua.gr>`_, Athens, Greece, 2008
   - Concentrations: Mathematical Analysis and Statistics
   - Advisor: `Vassilis Papanicolaou <http://www.math.ntua.gr/%7Epapanico/>`_.
   - Thesis: Pricing European Call Options under Proportional Transaction
     Costs.

Fellowships and Honors
^^^^^^^^^^^^^^^^^^^^^^

+ 2008-2009, Olin Fellowship, Cornell University.
+ 2003-2005, Award for best student performance in mathematics, National
  Technical University of Athens.
+ 2004-2005, Award for best student performance, State Scholarship Foundation
  (IKY).
+ 2003-2005, Award for best student performance, State Scholarship Foundation
  (IKY)
+ 2003, National Bank of Greece award for high-school students.


For more details, download Prof. Bilionis' :download:`CV <ib_cv.pdf>`.

.. _graduate_students:

Graduate Students
-----------------

Well, we just moved to Purdue...
If you are a prospective student, you should checkout our :ref:`open_positions`.

.. image:: _images/collaborators/jesper.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Jesper Toft Kristensen <http://www.jespertoftkristensen.com/JTK/Welcome.html>`_
| `Applied and Engineering Physics <http://www.aep.cornell.edu>`_
| `Cornell University <http://www.cornell.edu>`_, Ithaca, NY, USA
|
|
|
|
|
|
|

.. image:: _images/collaborators/tsilifis.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Panagiotis Tsilifis <http://www-scf.usc.edu/~tsilifis/>`_
| `Department of Mathematics <http://dornsife.usc.edu/mathematics/>`_
| `University of Southern California <http://www.usc.edu>`_, Los Angeles, CA, USA
|
|
|
|
|
|
|

.. _postdocs:

Postdocs
--------

Again, we just moved to Purdue...
Feel free to checkout our :ref:`postdoc_openings` periodically.

Collaborators
-------------

Here is a list of our collaborators:

.. image:: _images/collaborators/zabaras.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Prof. Nicholas Zabaras <www.zabaras.com>`_
| `University of Warwick <http://www2.warwick.ac.uk>`_, UK
|
|
|
|
|
|
|
|

.. image:: _images/collaborators/emil.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Dr. Emil Constantinescu <http://www.mcs.anl.gov/~emconsta/>`_
| `Laboratory for Advanced Numerical Simulations <http://www.mcs.anl.gov/research/LANS>`_
| `Argonne National Laboratory <http://www.anl.gov/>`_, Lemont, IL, USA
|
|
|
|
|
|
|

.. image:: _images/collaborators/mihai.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Prof. Mihai Anitescu <http://www.mcs.anl.gov/~anitescu/>`_
| `Laboratory for Advanced Numerical Simulations <http://www.mcs.anl.gov/research/LANS>`_
| `Argonne National Laboratory <http://www.anl.gov/>`_, Lemont, IL, USA
| and
| `Department of Statistics <http://www.stat.uchicago.edu/people/faculty.shtml>`_
| `University of Chicago <http://www.uchicago.edu>`_, Chicago, IL, USA
|
|
|
|

.. image:: _images/collaborators/stelios.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Prof. Phaedon-Stelios Koutsourelakis <http://www.professoren.tum.de/en/koutsourelakis-phaedon-stelios/>`_
| `Mechanical Engineering <http://www.mw.tum.de/index.php?lang=en>`_
| `Technical University of Munich <http://www.tum.de>`_, Germany
|
|
|
|
|
|
|

.. image:: _images/collaborators/guang.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Prof. Guang Lin <http://www.math.purdue.edu/people/bio/lin491>`_
| `Mathematics <http:http://www.math.purdue.edu>`_
| and
| `Mechanical Engineering <https://engineering.purdue.edu/ME>`_,
| `Purdue University <http://www.purdue.edu>`_, West Lafayette, IN, USA
|
|
|
|
|

.. image:: _images/collaborators/alex.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| Dr. Alex Konomi
| `Pacific Northwest National Laboratory <https://www.pnnl.gov>`_, Richland, WA, USA
|
|
|
|
|
|
|
|

.. image:: _images/collaborators/beth.jpg
    :width: 100px
    :align: left
    :alt: alternate text

| `Beth A. Drewniak <http://www.evs.anl.gov/about-evs/staff/detail/index.cfm?/Drewniak/Beth>`_
| `Environmental Science Division <www.evs.anl.gov>`_
| `Argonne National Laboratory <www.anl.gov>`_
|
|
|
|
|
|
|

